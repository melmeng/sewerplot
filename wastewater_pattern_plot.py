'''
read the wastewater.csv exported from InfoWorks and,
1. a pattern_summary.csv, which shows
	PROFILE_DESCRIPTION	FLOW	PROFILE_NUMBER	weekday_max	weekday_mean	weekend_max
0	NE01	0.999894	1	1.76	0.999979167	1.807
1	NE02	0.999894	2	1.543	0.999982639	1.756
2	NE03	0.999894	3	1.278	1.000010417	0.534
3	NE04	0.999894	4	1.979	0.999982639	2.095
4	NE05	0.999894	5	1.698	1.000020833	2.211
5	NE06	0.999894	6	1.947	0.999993056	1.77
6	NE07	0.999894	7	1.847	1	1.76
7	NE08	0.999894	8	1.638	1.000003472	1.923
8	NE09	0.999894	9	1.616	1.000003472	1.712
9	NE10	0.999894	10	1.748	1.000010417	2.534
10	NE11	0.999894	11	1.549	0.999979167	1.816
11	NE12	0.999894	12	1.738	1.000027778	2.005
12	NE13	0.999894	13	1.882	0.999972222	2.759

2. each pattern is saved as a csv file
3. each meter basin has a plot in the plot folder
'''

import iw_wastewater
import pandas as pd
import os

        


iw_csv = './workspace/wastewater/wastewater.csv'
out_folder  = './workspace/wastewater/profile'
profile_path = './workspace/wastewater/profile'
iw_wastewater.convert(iw_csv, profile_path)
out_folder  = './workspace/wastewater/plot_2fig'

iw_wastewater.plot(out_folder, profile_path)

out_folder  = './workspace/wastewater/plot_1fig'
iw_wastewater.plot_one(out_folder, profile_path)

out_folder  = './workspace/wastewater'
iw_wastewater.summarize(out_folder, profile_path)

