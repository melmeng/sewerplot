"""

Folder structure
================
workspace
 - input
    - events.csv
    - obs
        - obs_depth.csv
        - obs_flow.csv
        - obs_vel.csv
    - sim
        - XXXX_depthXXX.csv
        - XXX_velXXX.csv
        - XXX_flowXXX.csv
 - output

Observed Data: obs_XXX.csv
==========================

Imported from InfoWorks Survey Group as csv file., remove the top rows exported from InfoWorks, and also remove all the leading spaces in the meter location names.
Sample CSV data.
P_DATETIME	CR_01	CR_02	CR_03	CR_04	CR_05	CR_06	CR_07
3/1/2013 0:00	1.7225	1.529167	2.99	1.882999	0.79	0.643333	0.441667
3/1/2013 0:05	1.7225	1.529167	2.99	1.886001	0.78	0.643333	0.441667
3/1/2013 0:10	1.7225	1.524167	2.97	1.887999	0.79	0.643333	0.441667


Simulation Data: XXX_velXXX.csv
===============================
Exported from InfoWorks results as csv file, the 2nd row are the units and will be ignored

events.csv
==========
Sample csv format,
fm	link	rainfall	U/S End	dia	event0404	event0324
CR_01	277515.1	3	No	12	4/4/2013  6:00 - 4/6/2013 6:00	03/24/2013 0:00 - 03/26/2013 0:00
CR_02	277516.1	16	No	12
CR_03	100979.1	6	No	12
CR_04	100683.1	16	No	12
CR_05	100750.1	2	No	12
CR_06	119791.1	2	No	12
CR_07	114685.1	8	No	12
CR_08	107529.1	8	No	12

All events columns must start with "event", and the value should follow "start-end". Diameter should be in feet, used to tell if it is surcharged or not.
"""
import sys
import pandas as pd
import sewerplot as sp
import matplotlib.pyplot as plt
import os
from dateutil.parser import *
from util import read_events
import logging


logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)




def merge_csv(src_csv_list, out_csv, skiprows=[1]):
    '''merge all the depth csv into one csv file, assuing the second row is unit'''
    df = None
    for f in src_csv_list:
        logging.info('adding: %s' % f)
        if df is None:
            df = pd.read_csv(f, parse_dates=True, index_col=0, skiprows=skiprows)
        else:
            df = df.append(pd.read_csv(f, parse_dates=True, index_col=0, skiprows=skiprows))
    df['idx'] = df.index
    df = df.sort('idx')
    del df['idx']
    df.to_csv(out_csv)
    logging.info('successfully merged csv: %s' % out_csv)

def merge_results(src_folder, out_folder):
    paths = {}
    for p in ['depth', 'flow', 'vel', 'rainfall']:
        #TOOD: check rainfall naming convention
        for f in os.listdir(os.path.join(src_folder, 'sim')):
            if '_%s.csv' % p in f:
                paths.setdefault(p, [])
                paths[p].append(os.path.join(src_folder, 'sim/%s' % f))
    for p in ['depth', 'flow', 'vel', 'rainfall']:
        logging.info('*'*50)
        logging.info('merging %s results:' % p)
        path_list = paths.get(p)
        if path_list:
            merge_csv(path_list, os.path.join(out_folder, '%s.csv' % p))
        else:
            logging.warning('no %s data found.' % p)
        
def merge_summary(out_folder, events):
    '''
    merge the summary for each event and parameter into one table
    '''
    df_obs = pd.read_csv(os.path.join(out_folder, 'obs_summary.csv'))
    df_obs = df_obs.set_index('link')
    df_sim = pd.read_csv(os.path.join(out_folder, 'sim_summary.csv'))
    df_sim = df_sim.set_index('link')
    
    

    for p in ['flow', 'volume', 'depth', 'vel']:
        out_csv = os.path.join(out_folder, '%s_summary.csv' % p)
        df_summary = df_obs.loc[:, ['fm']]
        for event in events['events']:
            if p=='volume':
                p2 = p
            else:
                p2 = 'peak_%s' % p
            
            col = '%s_%s' % (event.replace('event', ''), p2)
            df_summary['obs_%s' % event.replace('event', '')] = [df_obs[col][x] for x in df_summary.index]
            df_summary['sim_%s' % event.replace('event', '')] = [df_sim[col][x] for x in df_summary.index]

            df_summary['error_%s' % event.replace('event', '')] = df_summary['sim_%s' % event.replace('event', '')] - df_summary['obs_%s' % event.replace('event', '')]
            df_summary['errorp_%s' % event.replace('event', '')] = (df_summary['sim_%s' % event.replace('event', '')] - df_summary['obs_%s' % event.replace('event', '')])/df_summary['obs_%s' % event.replace('event', '')]*100
        df_summary.to_csv(out_csv, index_label='link')
            
def depth_summary(csv_path, event_path):
    '''
    identify surcharged pipes
    '''
    logging.info('depth summary:%s' % csv_path)
    df_depth = pd.read_csv(csv_path, index_col=1)
    #convert all numbers to string so that it can be marked as '--'
    for col in df_depth.columns:
        df_depth[col] = [str(x) for x in df_depth[col]]
    df_depth_surcharged = df_depth.copy()
    df_event = pd.read_csv(event_path, index_col=0)
    df_depth['dia'] = [df_event.loc[x]['dia'] for x in df_depth.index]
    df_depth_surcharged['dia'] = [df_event.loc[x]['dia'] for x in df_depth_surcharged.index]


    for idx in df_depth.index:
        row = df_depth.loc[idx]
        
        dia = float(df_event.loc[idx]['dia'])
        logging.debug('dia=%s' % dia)
        for col in df_depth.columns:
            if 'obs' in col:
                logging.debug('process col: %s' % col)
                try:

                    depth = float(row[col])


                    if depth>dia:
                        df_depth.ix[idx, col] = '--'
                        df_depth.ix[idx, col.replace('obs', 'error')] = '--'
                        df_depth.ix[idx, col.replace('obs', 'errorp')] = '--'
                        logging.info('%s: %s, surcharged' % (idx, col))
                    else:
                        df_depth_surcharged.ix[idx, col] = '--'
                        df_depth_surcharged.ix[idx, col.replace('obs', 'error')] = '--'
                        df_depth_surcharged.ix[idx, col.replace('obs', 'errorp')] = '--'
                        logging.info('%s: %s, not surcharged' % (idx, col))
                except:
                    logging.warning(sys.exc_info()[0])
    df_depth.to_csv(csv_path.replace('depth_summary.', 'depth_not_surcharged_summary.'))
    df_depth_surcharged.to_csv(csv_path.replace('depth_summary.', 'depth_surcharged_summary.'))
                               
def obs_stat(src_folder, out_folder, events, timestep):
    logging.info('summarize simulation results: %s->%s, timestep=%s' % (src_folder, out_folder, timestep))
    dfs = {}
    df_summary = None
    out_csv = os.path.join(out_folder, 'obs_summary.csv')
    for event in events['events']:
        
        start, end = events['events'][event]
        for p in ['flow', 'volume', 'depth', 'vel']:
            csv_file = os.path.join(src_folder, 'obs/obs_%s.csv' % p)
            fns = {'max': 'max'}
            FN = sp.ts.FN
            if p=='volume':
                #flow and volume use the same file
                csv_file = os.path.join(src_folder, 'obs/obs_flow.csv')
                fns = {'max': 'max', 'sum': 'sum'}
    
            
            #reuse the df
            dfs.setdefault(p, None)
            if dfs[p] is None:
                df = pd.read_csv(csv_file, parse_dates=True, index_col=0)
                links = events['link'].keys()
                idx = events['link'].values()
                df = df.copy()#df.loc[:, idx]
                dfs[p] = df
                logging.info('Reading Data from Source:%s' % csv_file)
            else:
                logging.info('Reading Data from memory:%s' % csv_file)

            df = dfs[p]
            
            #do the summary
            df_temp = sp.ts.stat(df, start, end, fns, out_csv, FN)
            if df_summary is None:
                df_summary = df_temp
                
            
            if p=='volume':
                values = [df_temp['sum'][x]*timestep/60.0/60.0/24.0 for x in df_summary.index]
                df_summary['%s_volume' % event.replace('event', '')] = values
            else:
                values = [df_temp['max'][x] for x in df_summary.index]
                df_summary['%s_peak_%s' % (event.replace('event', ''), p)] = values
                if 'max' in df_summary.columns:
                    del df_summary['max']
    df_summary['link'] = [events['fm'][x] for x in df_summary.index]
    df_summary.to_csv(out_csv, index_label='fm')
    
    logging.info('Volume summary saved to: %s' % out_csv)
            
  
def sim_stat(src_folder, out_folder, events, timestep):
    logging.info('summarize simulation results: %s->%s, timestep=%s' % (src_folder, out_folder, timestep))
    dfs = {}
    df_summary = None
    out_csv = os.path.join(out_folder, 'sim_summary.csv')
    for event in events['events']:
        
        start, end = events['events'][event]
        for p in ['flow', 'volume', 'depth', 'vel']:
            csv_file = os.path.join(src_folder, '%s.csv' % p)
            fns = {'max': 'max'}
            FN = sp.ts.FN
            if p=='volume':
                #flow and volume use the same file
                csv_file = os.path.join(src_folder, 'flow.csv')
                fns = {'max': 'max', 'sum': 'sum'}
    
            
            #reuse the df
            dfs.setdefault(p, None)
            if dfs[p] is None:
                df = pd.read_csv(csv_file, parse_dates=True, index_col=0)
            else:
                df = dfs[p]
            
            #do the summary
            df_temp = sp.ts.stat(df, start, end, fns, out_csv, FN)
            if df_summary is None:
                df_summary = df_temp
                
            
            if p=='volume':
                values = [df_temp['sum'][x]*timestep/60.0/60.0/24.0 for x in df_summary.index]
                df_summary['%s_volume' % event.replace('event', '')] = values
            else:
                values = [df_temp['max'][x] for x in df_summary.index]
                df_summary['%s_peak_%s' % (event.replace('event', ''), p)] = values
                if 'max' in df_summary.columns:
                    del df_summary['max']
    df_summary['fm'] = [events['link'].get(str(x)) for x in df_summary.index] #FIXME: if not found in events, then meter is ""
    df_summary.to_csv(out_csv, index_label='link')
    logging.info('Volume summary saved to: %s' % out_csv)



def run(ws):
    src_folder = os.path.join(ws, 'input')
    out_folder = os.path.join(ws, 'output')
    
    event_csv = os.path.join(src_folder, 'events.csv')
    # events = read_events(event_csv)

    # merge_results(src_folder, src_folder)
    # sim_stat(src_folder, out_folder, events, 30*5) # check infoworks setting for timestep
    # obs_stat(src_folder, out_folder, events, 60*5) #5min, in seconds, manually picked
    # merge_summary(out_folder, events)
    csv_path = os.path.join(out_folder, 'depth_summary.csv')
    depth_summary(csv_path, event_csv)
    

if __name__ == '__main__':

    ws = r'C:\Users\men73570\Desktop\papps\Documents\projects\2014 Raleigh\Report\Appendix E\validation'
    run(ws)

    