import unittest
import sewerplot as sp
import matplotlib.pyplot as plt
import os
import plots
import pandas as pd

import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)



class TestSewerPlotTestCase(unittest.TestCase):
    def setUp(self):
        ws = '../workspace'
        sewer = sp.SewerPlot(ws)
        self.sewer = sewer
        self.ws = ws

    def tearDown(self):
        pass
    
    def testHist(self):
        csv_path = os.path.join(self.ws, 'test/plots/hist_plot.csv')
        plots.hist_plot_csv(csv_path)
    def testScatter(self):
        pass