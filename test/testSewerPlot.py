import unittest
import sewerplot as sp
import matplotlib.pyplot as plt
import pandas as pd
import os

import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)



class TestSewerPlotTestCase(unittest.TestCase):
    def setUp(self):
        ws = '../workspace'
        ws = sp.workspace.Workspace(ws)
        self.workspace = ws

    def tearDown(self):
        pass
    
    def testWorkspace(self):
        ws = self.workspace
        print ws.workspace
        print ws.data
    def testProcessRunGroup(self):
        ws = self.workspace
        ws.process_run_group()
        
        f = os.path.join(ws.workspace, 'run_group/wwf/_depth.csv')
        df = pd.read_csv(f, index_col=0, parse_dates=True)
        df2 = df.loc[:, df.columns[:3]]
        df2.plot(subplots=True)
        plt.show()
        
        