import unittest
import sewerplot as sp
import matplotlib.pyplot as plt
import os

import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)



class TestSewerPlotTestCase(unittest.TestCase):
    def setUp(self):
        ws = '../workspace'
        sewer = sp.SewerPlot(ws)
        self.sewer = sewer
        self.ws = ws

    def tearDown(self):
        pass
    
    def testStats(self):
        csv_file = os.path.join(self.ws, 'test/ts/stat_test.csv')
        start = '1/5/2013'
        end = '1/20/2013'
        
        fns = {'sum': 'sum', 'peak': 'max', 'volume': 'vol'}
        FN = sp.ts.FN
        FN['vol'] = lambda x: x.sum()*10
        out_csv = os.path.join(self.ws, 'test/ts/stat_test_ouptut.csv')
        
        df = sp.ts.stat(csv_file, start, end, fns, out_csv, FN)
        
        df['volume2'] = df['sum']*10
        del df['sum']
        df.to_csv(out_csv)

        