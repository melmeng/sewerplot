import datetime
import csv
import pandas as pd
import numpy as np
import logging
import matplotlib.pyplot as plt
import pylab
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date


from matplotlib.ticker import FuncFormatter
import os
from util import *
from scipy.interpolate import interp1d
import matplotlib.dates as mdates

##CONVERT iw export waste water patterns into individual pattern files.
##
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)
logging.info('starting logging')

def read_iw_wastewater(iw_csv):
    with open(iw_csv) as f:
        reader = csv.reader(f)
        
        profile = None
        i = 0
        p_line = '' #previous line
        c_line = '' #current line
        for l in reader:
                        
            i+=1
            s = l[0]
            if s=='PROFILE_NUMBER':
                c_line = 'p_number header'
                profile = {}
            elif s=='CALIBRATION_WEEKDAY':
                c_line = 'weekday title'
            elif s=='CALIBRATION_WEEKEND':
                c_line = 'weekend title'
            elif s=='CALIBRATION_MONTHLY':
                c_line = 'monthly title'
            
            
            if p_line=='p_number header':
                c_line = 'row'
                profile['summary'] = dict(zip(header, l))
                rows = []
            elif p_line=='weekday title':
                c_line = 'weekday header'
                header = l
                rows = []
            elif p_line=='weekend title':
                c_line = 'weekend header'
                header = l
                rows = []
            elif p_line=='weekend header':
                c_line = 'row'
                
            elif p_line=='weekday header':
                c_line = 'row'
                
            
            if 'header' in c_line:
                header = l
                row = []
            elif c_line=='weekend title':
                profile['weekday'] = {'header': header, 'rows': rows}
                header = None
                rows = None
                
            elif c_line=='row':
                rows.append(l)  
            
            elif c_line=='monthly title':
                profile['weekend'] = {'header': header, 'rows': rows}
                header = None
                rows = None
                yield profile
                profile = None
                p_line = ''
                c_line = ''
            
            p_line = c_line
            
            
def convert(iw_csv, out_folder):
    summary = []
    for p in read_iw_wastewater(iw_csv):
        summary.append(p['summary'])
        for pattern in ['weekday', 'weekend']:
            with open(os.path.join(out_folder, '%s_%s.csv' % (p['summary']['PROFILE_NUMBER'], pattern)), 'w') as o:
                writer = csv.writer(o, lineterminator='\n')
                writer.writerow(p[pattern]['header'])
                for row in p[pattern]['rows']:
                    writer.writerow(row)
    with open(os.path.join(out_folder, 'summary.csv'), 'w') as o:
        writer = csv.writer(o, lineterminator='\n')
        h = summary[0].keys()
        writer.writerow(h)
        for r in summary:
            row = [r[x] for x in h]
            writer.writerow(row)
        
def plot_one(out_folder, profile_path):
    f = os.path.join(profile_path, 'summary.csv')
    profiles = pd.read_csv(f)
    
    for idx, r in profiles.iterrows():
        fig = plt.figure(figsize=(8, 4))
        
        fm = r['PROFILE_DESCRIPTION']
        pn = r['PROFILE_NUMBER']
        logging.info('%s: %s' % (fm, pn))
        i = 0
        ymax = 0
        for d in ['weekend', 'weekday']:
            f = os.path.join(profile_path, '%s_%s.csv' % (pn, d))
            p1 = pd.read_csv(f, index_col=0, parse_dates=True)
            ymax = max(p1['FLOW'].max(), ymax)
            ax = fig.add_subplot(1, 1, 1)
        for d in ['weekend', 'weekday']:
            i+=1
            
            f = os.path.join(profile_path, '%s_%s.csv' % (pn, d))
            p1 = pd.read_csv(f, index_col=0, parse_dates=True)
            m = p1['FLOW'].mean()
            p1['avg'] = m
            
            ax.set_ylabel('Factor')
            ax.set_xlabel('Time')
            ax.set_ylim(0, 1.3*ymax)
            ax.set_title(fm)
            ax.grid(True)
            ax.xaxis.set_minor_locator( HourLocator() )
            ax.xaxis.set_major_locator( HourLocator(interval=4) )
        
            def dlabel(x, pos):
                x = num2date(x)
                return x.strftime('%H')
            ax.xaxis.set_major_formatter(FuncFormatter(dlabel))
            if d=='weekend':
                style = 'g-'
                style2 = 'g--'
            else:
                style = 'r-'
                style2 = 'r--'
                
            ax.plot(p1.index, p1['FLOW'], style, lw=3,  label='%s Pattern' % d.title())
            ax.plot(p1.index, p1['avg'], style2, lw=2,  label='%s Avg.(%.2f)' % (d.title(), m))
            ax.legend(loc='best', fontsize='small')
            
        fpath = os.path.join(out_folder, '%s.png' % fm)
        fig.savefig(fpath, dpi=150, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()
        pylab.close()
        
def plot(out_folder, profile_path):
    f = os.path.join(profile_path, 'summary.csv')
    profiles = pd.read_csv(f)
    
    for idx, r in profiles.iterrows():
        fig = plt.figure(figsize=(8, 4))
        
        fm = r['PROFILE_DESCRIPTION']
        pn = r['PROFILE_NUMBER']
        logging.info('%s: %s' % (fm, pn))
        i = 0
        ymax = 0
        for d in ['weekend', 'weekday']:
            f = os.path.join(profile_path, '%s_%s.csv' % (pn, d))
            p1 = pd.read_csv(f, index_col=0, parse_dates=True)
            ymax = max(p1['FLOW'].max(), ymax)
            
        for d in ['weekend', 'weekday']:
            i+=1
            ax = fig.add_subplot(1, 2, i)
            f = os.path.join(profile_path, '%s_%s.csv' % (pn, d))
            p1 = pd.read_csv(f, index_col=0, parse_dates=True)
            m = p1['FLOW'].mean()
            p1['avg'] = m
            
            ax.set_ylabel('Factor')
            ax.set_xlabel('Time')
            ax.set_ylim(0, 1.5*ymax)
            ax.set_title(d.title())
            ax.grid(True)
            ax.xaxis.set_minor_locator( HourLocator() )
            ax.xaxis.set_major_locator( HourLocator(interval=4) )
        
            def dlabel(x, pos):
                x = num2date(x)
                return x.strftime('%H')
            ax.xaxis.set_major_formatter(FuncFormatter(dlabel))
            if d=='weekend':
                style = 'g-'
            else:
                style = 'r-'
                
            ax.plot(p1.index, p1['FLOW'], style, lw=3,  label='Flow Pattern')
            ax.plot(p1.index, p1['avg'], 'b--', lw=2,  label='Daily Average(%.1f)' % m)
            ax.legend(loc='best', fontsize='small')
            
        fpath = os.path.join(out_folder, '%s.png' % fm)
        fig.savefig(fpath, dpi=150, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()
        pylab.close()


def summarize(out_folder, profile_path):
    f = os.path.join(profile_path, 'summary.csv')
    profiles = pd.read_csv(f)
    weekend_max = []
    weekday_max = []
    weekend_mean = []
    weekday_mean = []
    for idx, r in profiles.iterrows():
        
        fm = r['PROFILE_DESCRIPTION']
        pn = r['PROFILE_NUMBER']
        logging.info('%s: %s' % (fm, pn))
        i = 0
        
        for d in ['weekend', 'weekday']:
            f = os.path.join(profile_path, '%s_%s.csv' % (pn, d))
            p1 = pd.read_csv(f, index_col=0, parse_dates=True)
            if d=='weekend':
                weekend_max.append(p1['FLOW'].max())
                weekend_mean.append(p1['FLOW'].mean())
            else:
                weekday_max.append(p1['FLOW'].max())
                weekday_mean.append(p1['FLOW'].mean())
            
    profiles['weekday_max'] = weekday_max
    profiles['weekday_mean'] = weekday_mean
    profiles['weekend_max'] = weekend_max
    profiles['weekend_max'] = weekend_max
    f = os.path.join(out_folder, 'pattern_stats.csv')
    profiles.to_csv(f)
    logging.info('pattern summary created:%s' % f)