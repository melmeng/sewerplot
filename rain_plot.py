import rain
import os
import pandas as pd
import logging
import math
import matplotlib.pyplot as plt
import pylab
import dateutil
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)



def main(ws):
    f = os.path.join(ws, 'rain_plot/input.csv')
    
    input_csv = pd.read_csv(f, parse_dates=True)
    
    db = {}
    plot_data = []
    for idx, row in input_csv.iterrows():
        row = dict(row)
        logging.debug(row)
        for fld in row:
            try:
                if math.isnan(row[fld]):
                    row[fld] = None
            except:
                pass
        f = row['csv_file']
        
        rg = str(row['rg'])
        timestep = row['timestep']
        
        timestep = pd.DateOffset(seconds=timestep)
        label  = row['label']
        ymax = row['ymax']
        grid = row['grid']
        subplot = row['subplot']
        start = row['start']
        end = row['end']
        if rg:
            if not (f in db):
                src_csv = os.path.join(ws, 'rain_plot/input/%s' % f)
                db[f] = pd.read_csv(src_csv, index_col=0, parse_dates=True)
            
            df = db[f]
            start = min(df.index).strftime('%m/%d/%Y %H:%M')
            end = max(df.index).strftime('%m/%d/%Y %H:%M')
            if start and end:
                start = dateutil.parser.parse(start).strftime('%m/%d/%Y %H:%M')
                end = dateutil.parser.parse(end).strftime('%m/%d/%Y %H:%M')

                df = df.loc[start:end]
            
                

            if timestep:
                df = df.asfreq(timestep)
                
            
            
            plot_data.append([df.copy(), rg, start, end, timestep, label, ymax, subplot])
        
    ct = len(plot_data)
    
    if plot_data[0][-1]=='Y':
        #seperate plots
        fig_one = plt.figure(figsize=(11,2*ct))
        plt.subplots_adjust(hspace = .1)

    
    i = 0
    for df, rg, start, end, timestep, label, ymax, subplot in plot_data:
        i += 1
        if plot_data[0][-1]=='Y':
            ax = fig_one.add_subplot(ct, 1, i)
            
                
            rain.rain_plot.rain_plot(df, rg, ax, start, end, timestep, label, ymax, grid)
            rain.rain_plot.hourlyxlabel(ax)
            if i<ct:
                rain.rain_plot.hide_xticklabels(ax)
        else:
            fig = plt.figure(figsize=(11, 2))
            ax_one = fig.add_subplot(1,1,1)
            
            ax = ax_one
            rain.rain_plot.rain_plot(df, rg, ax, start, end, timestep, label, ymax, grid)
            rain.rain_plot.hourlyxlabel(ax)
            
            fpath = 'rain_plot/output/rg%s_%s.png' % (rg, i)
            fpath = os.path.join( ws, fpath)
            fig.savefig(fpath, dpi=150, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()
            pylab.close()
    
    if plot_data[0][-1]=='Y':
        fpath = os.path.join( ws, 'rain_plot/output/all_in_one.png')
        fig_one.savefig(fpath, dpi=150, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()
        pylab.close()
        
            
if __name__=='__main__':
    ws = './workspace'
    main(ws)
    