import pandas as pd
import matplotlib.pyplot as plt

import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def subcatchments_flow_projection2(f, f_out):
    """
    f: subcatchment csv file
    
    wwf_gwi: ground water infiltration, base_flow field in IW, the calibrated value
    wwf_bsf: base sanitary flow, additional_faul_flow field in IW, the calibrated value
    unit_hydrograph_id: flow monitoring basin
    
    subcatchment_id,node_id,contributing_area,wastewater_profile,wwf_gwi,wwf_bsf,rainfall_profile,unit_hydrograph_id,EMP2010,EMP2013,EMP2015,EMP2025,EMP2030,POP2010,POP2013,POP2015,POP2020,POP2025,POP2030
    10091,101921,0.634852509,50,78,400,3,WN_06,0,0,0,0.166666666,0.166666666,3.294451149,3.405003201,3.478704569,7.744791638,8.11718747,8.489583302
    10092,101921,0.946418957,50,0,600,3,WN_06,0,0,0,0.166666666,0.166666666,4.529200937,4.681187546,4.782511952,7.744791638,8.11718747,8.489583302
    
    f_out: csv file with flow projection calculated
    
    gwi_XXXX: gwi for year XXXX, eg. gwi_2013, the GWI of year 2013
    bsf_XXXX: dwf for year XXXX
    ca_XXXX: contribution area for year XXXX
    
    """
    #read the subcatchment table
    df = pd.read_csv(f)
    
    #Flow projection for year 2013
    
    #Calibration was done in March, April and May , 2013, considered to be wet season of the year
    # A factor was calculated for Neuse WWTP and Smith WWTP to bring the calibrated value down to match the projected value 
    def fn(x): # a helper function
        if 'SC' in x: #smith creek is 0.85
            return 0.85
        else:#neuse is 0.98
            return 0.98
    #apply 0.85 and 0.98 to each subcatchment
    factor = [fn(x) for x in df['unit_hydrograph_id']]
    
    #calculate how to divide the increased flow between gwi and dwf
    df['bsf_2013'] = factor*df['wwf_bsf']
    df['gwi_2013'] = factor*df['wwf_gwi']
    
    bsf2013 = df['bsf_2013'].sum() #total base sanitary flow
    gwi2013 = df['gwi_2013'].sum() #total ground water infiltration
    dwf2013 = bsf2013 + bsf2013            #total dry weather flow
    
    #Calculate Flow Increase
    for year in ['2015', '2020', '2025']:
        #formula: flow increase 2015= [85*(POP2015-POP2013) + 10*(EMP2015-EMP2013)] 
        df['flow_increase_%s' % year] = (df['POP%s' % year] - df['POP2013'])*85 + (df['EMP%s' % year] - df['EMP2013'])*10
    
        #increase GWI, bsf by a factor to match the projected flow
        #using the base year gwi, bsf to allocate additional flow
        df['gwi_%s' % year] = df['gwi_2013'] + df['flow_increase_%s' % year]*(gwi2013/dwf2013)  
        df['bsf_%s' % year] = df['bsf_2013'] + df['flow_increase_%s' % year]*(bsf2013/dwf2013)
        #calculate contribution area
        gwi = df['gwi_%s' % year].sum() #total gwi of current year
        bsf = df['bsf_%s' % year].sum() #total bsf of current year
        dwf = gwi + bsf                 #total dwf of current year
        df['ca_%s' % year] = dwf/dwf2013*df['contributing_area'] 
    
    df.to_csv(f_out)
    logging.info('flie saved to:%s' % f_out)

if __name__=='__main__':
    f = './workspace/subcatchment/subcatchment.csv'
    f_out = './workspace/subcatchment/subcatchment_out.csv'
    subcatchments_flow_projection2(f, f_out)