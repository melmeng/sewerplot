import sewerplot as sp

import os


if __name__=='__main__':
    #path to the observed flow file
    workspace = './workspace'
    model = sp.SewerPlot(workspace)
    fpath = model['obs']['flow']
    
    start, end = '3/1/2013', '4/11/2013' #start and end dates
    meters = ['CR_%02d' % x for x in range(1, 8)] #meters CR_01 - CR_08
    
    #folder where the output image goes to
    out_folder = os.path.join(workspace, 'check_flow_data')
    if os.path.exists(out_folder):
        pass
    else:
        os.makedirs(out_folder)
    sp.check_flow_data.check_data(fpath, start, end, meters)
