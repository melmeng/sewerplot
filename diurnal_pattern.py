import sewerplot as sp

import os

if __name__=='__main__':
    #path to the observed flow file
    workspace = './workspace'
    model = sp.SewerPlot(workspace)
    fpath = model['obs']['flow']
    
    days = [1,2,3,4,5]  #monday is 1, Sunday is 7
    #days = [6,7]         #weekend
    start, end = '3/1/2013', '3/11/2013' #start and end dates
    meters = ['CR_%02d' % x for x in range(1, 8)] #meters CR_01 - CR_08
    std_l = 1 
    std_h = 1
    #the plot and the data will be saved in the folder.
    outfolder = os.path.join(workspace, 'diurnal_pattern')
    if os.path.exists(outfolder):
        pass
    else:
        os.makedirs(outfolder)
    sp.diurnal_pattern.diurnal_pattern(fpath, start, end, meters, days, std_l, std_h, outfolder)