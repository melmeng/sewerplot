import pandas as pd
import rain_plot

import logging
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)

def resample(src_csv, freq, target_freq, target_csv):
    """
    source:
    date, rainfall, etc
    
    rainfall in (in/hr), datetime must be first column
    currently only works for down sample, from 5min to 15min 
    """
    
    df = pd.read_csv(src_csv, index_col=0, parse_dates=True)
    df = df.asfreq(freq)
    df = df.fillna(0)
    df = df.resample(target_freq, how='mean', fill_method='backfill')
    df.to_csv(target_csv)
