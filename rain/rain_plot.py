import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import pylab
import numpy as np
import matplotlib

logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.INFO)


class rain:
    def __init__(self, workspace, f, timestep):
        '''
        timestemp: int minutes
        '''
        f = os.path.join(workspace, f)
        df = pd.read_csv(f, parse_dates=True, index_col=0)
        df = df.asfreq('%sMin' % timestep)
        self.df = df
        self.workspace = workspace
        self.timestep = timestep
    
    def plot(self, rg, ax, start, end, label=None, ymax=None, grid=True):
        rain_plot(self.df, rg, ax, start, end, self.timestep, label, ymax, grid)
    def plot_events(self, events, rgs, ymax=None, folder=None):
        ymax1 = ymax
        for start, end in events:
            rain = self
            ct = len(rgs)
            fig = plt.figure(figsize=(11,2*ct))
            plt.subplots_adjust(hspace = .1)
            i = 0
            d = rain.df.loc[start:end, [str(x) for x in rgs]]
            ymax = ymax1 or max(d.max())*1.3
            for rg in rgs:
                rg = str(rg)
                i+=1
                ax = fig.add_subplot(ct, 1, i)
                label = None
                rain.plot(rg, ax, start, end, label=label, ymax=ymax, grid=True)
                if i<ct:
                    hide_xticklabels(ax)
                else:
                    hourlyxlabel(ax)
            if not folder:
                folder = os.path.join(rain.workspace, 'output')
            
            fpath = os.path.join( folder, 'rain-%s-%s.png' % (start.replace('/', '.').replace(':', '.'), end.replace('/', '.').replace(':', '.')))
            fig.savefig(fpath, dpi=150, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()
            pylab.close()
    
def bar2line(r, timestep):
    '''
    r: time series
    timestep: int, seconds
    rainfall each time step is a period, so to plot it, it needs to be converted into a line, adding end points for each time step.
    
    A filtering is also done to only draw the boundary of the rainfall rather than each of it.
    '''
    data = {}
    for t, v in zip(r.index, r.values):
            
        if v>0:
            t0 = t - datetime.timedelta(seconds=timestep)
            if data.get(t0)==0:
                data[t0] = data[t0 - datetime.timedelta(seconds=0.001)]
            else: 
                data[t0] = 0

            t1 = t0 + datetime.timedelta(seconds=0.001)
            data[t1] = v
            t2 = t - datetime.timedelta(seconds=0.001)
            data[t2] = v
            data[t] = 0
    idx = sorted(data.keys())
    values = [data[x] for x in idx]
    if idx:
        df = pd.DataFrame(values, index=idx)
    else:
        #if there is no rainfall in that period of time, simply plot the whole thing
        df = r
    return df


def hide_xticklabels(ax):    
    pylab.setp(ax.get_xticklabels(), visible=False)
    pylab.setp(ax.xaxis.get_majorticklabels(), rotation=0 )


def hourlyxlabel(ax):
    ax.xaxis.set_minor_locator( HourLocator() )
    ax.xaxis.set_major_locator( HourLocator(interval=4) )

    def dlabel(x, pos):
        x = num2date(x)
        if x.hour==0:
            return '00:00\n' + x.strftime('%m/%d')
        else:
            return x.strftime('%H:%M')
    ax.xaxis.set_major_formatter(FuncFormatter(dlabel))
    
            
def rain_plot(df, rg, ax, start, end, timestep, label=None, ymax=None, grid=True):
    
    data = df[start:end][rg]
    timestep = timestep.kwds['seconds']
    
    logging.info('plot rain: %s, %s->%s, ts=%s seconds, ymax=%s' % (rg, start, end, timestep, ymax))
    tr = data.sum()*timestep/3600.0
    logging.debug(tr)
    data = bar2line(data, timestep)
    logging.debug(data.head(20))
    font={'family' : 'serif','color'  : 'grey', 'weight' : 'normal','size':12}
    ax.set_ylabel('Rain (in/hr)', font)
    
    label = label or 'RGP:%s, total:%.2f in' % (rg, tr)
    ax.fill(data.index, data.values, label=label)
    
    ymax = ymax or max(data.values)*1.5
    ax.set_ylim((0, ymax))
    ax.invert_yaxis()
    
    
    
    ax.set_xlim((datetime.datetime.strptime(start, '%m/%d/%Y %H:%M'), datetime.datetime.strptime(end, '%m/%d/%Y %H:%M')))
    ax.grid(grid)
    ax.legend(loc='lower right', fontsize='small', numpoints=1)


def replace_data(df, source_rg, target_rg, start, end):
    '''
    start, end both will re replaced.
    '''
    df = df.copy()
    col = '%s_replaced' % source_rg
    df[col] = df[source_rg]
    df[col][start:end] = df[target_rg][start:end]
    return df
    

    

def rain_event_summary(df, events, out_folder, timestep):
    '''
    given the events, do the summary for each event by getting statistics of rainfall of all gauges,
    peak
    depth
    duration
    and the statistics of 
    mean, std, max, min, duration
    '''
    
    YLABELS = {'depth': 'Total Depth(in)', 'duration': 'Duration(hr)', 'peak': 'Peak(in/hr)'}
    #get row statistics
    #get duration
    
    result = pd.DataFrame([0]*len(df.columns), index=df.columns, columns=['max'])
    
    for start, end in events:
        logging.info('Event: %s->%s' % (start, end))
        d = df.loc[start:end, df.columns]


        
        #get the duration
        duration = []
        for col in d.columns:
            idx = d[col][d[col]>0].index
            dt = (idx[-1] - idx[0]).total_seconds()/60.0/60.0
            duration.append(dt) # hours
            logging.info('%s(%s), %s->%s' % (col, dt, idx[0], idx[-1]))
        result['%s_duration' % start] = duration
        for fn in ['mean', 'max', 'min', 'std', 'sum']:
            result[fn] = getattr(d, fn)()
        result['%s_peak' % start] = result['max']
        result['%s_depth' % start] = result['sum']*timestep/60.0
        
        
        
        plt.show()
        
    print result.mean()
    data = {}
        
    
    for fn in ['mean', 'max', 'min', 'std', 'sum']:
        data[fn] = getattr(result, fn)()
    summary = pd.DataFrame(data)
    f = os.path.join(out_folder, 'summary_data.csv')
    logging.info('summary saved to: %s' % f)
    result.to_csv(f)
    
    f = os.path.join(out_folder, 'summary_all.csv')
    logging.info('all summary saved to: %s' % f)
    summary.to_csv(f)
    
    i = 0
    fig = plt.figure(figsize=(6,6))
    
    for fld in ['peak', 'depth', 'duration']:
        
        i+=1
        ct = len(events)
        for start, end in events:
            ax = fig.add_subplot(ct, 1, i)
            d = result.loc[:, [x for x in result.columns if fld in x]].copy()
            d.columns = [x.split('_')[0] for x in result.columns if fld in x]
            
            
            d.boxplot(ax=ax)
            ax.set_ylabel(YLABELS[fld])
            if i==3:
                pass
#                 tlabels = [start for start,end in events]
#                 print tlabels
#                 ax.set_xticks(xrange(ct), tlabels)
            else:
                ax.set_xticks([])
#                 pylab.setp(ax.get_xticklabels(), visible=False)
#                 pylab.setp(ax.xaxis.get_majorticklabels(), rotation=0 )
    plt.subplots_adjust(hspace = .1)
    plt.tight_layout()
    f = os.path.join(out_folder, 'rain_boxplot.png')
    fig.savefig(f, dpi=150, orientation='landscape')
    logging.info('figure saved: %s' % f)
    plt.clf()
    pylab.close()
    
            
   
def idw(profile_path, rain_path, gauge, use_gauges, exculde_data):
    """
    using the inverse distance weight average to calculate rain profile
    """
    df_profile = pd.read_csv(profile_path) #get the x, y 
    logging.info('profile loaded: %s' % profile_path)
    logging.debug(df_profile)
    df_rain = pd.read_csv(rain_path, parse_dates=True, index_col=0)
    logging.info('rain loaded: %s' % profile_path)
    for rg, [start, end] in exculde_data.items():
        df_rain[rg][start:end] = df_rain.loc[start:end, [rg]].applymap(lambda x: np.nan)[rg]
        logging.info('Data excluded: rg:%s, %s->%s' % (rg, start, end))
#         logging.debug(df_rain[rg][start:end])

    
    d = df_profile[df_profile['profile']==int(gauge)]
    
    
    x0 = float(d['x'])
    y0 = float(d['y'])
    
    logging.info('Gauge:%s, x:%s, y:%s' % (gauge, x0, y0))
    df_profile['d2'] = (df_profile.x - x0)**2 + (df_profile.y - y0)**2
    logging.info('D^2 results')
    logging.info(df_profile)

    
    idw_values = []
    for idx, row in df_rain.iterrows():
        logging.debug('-----weighting: %s' % idx)
        values = []
        weights = []
        for k in use_gauges:
            v = float(row[k])
            if np.isnan(v):
                pass
            else:
                d2 = float(df_profile[df_profile['profile']==int(k)]['d2'])
                values.append(float(row[k])/d2)
                weights.append(1/d2)
            
            
        logging.debug('Values: %s' % values)
        logging.debug('weights: %s' % weights)
        idw_values.append(sum(values)/sum(weights))
    df_rain['%s_idw' % gauge] = idw_values
    return df_rain