import rain
import os
import pandas as pd
import logging
import math
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def convert(ws):
    f = os.path.join(ws, 'resample_rain/resample_list.csv')
    
    source_list = pd.read_csv(f)
   
    for idx, row in source_list.iterrows():
        if row['source_csv']:
            src_csv = os.path.join(ws, 'resample_rain/source/%s' % row['source_csv'])
            if not math.isnan(row['target_csv']):
                out_csv = row['target_csv']
            else:
                out_csv = '%s_to_%s.csv' % (row['source_csv'].replace('.csv', ''), row['target_freq'])
                
            out_csv = os.path.join(ws, 'resample_rain/output/%s' % out_csv)
            
            logging.info('%s(%s)=>%s(%s)' % (src_csv, row['source_freq'], out_csv, row['target_freq']))
        
            rain.resample(src_csv, row['source_freq'], row['target_freq'], out_csv)
            
            logging.info('done')
            
            
if __name__=='__main__':
    ws = './workspace'
    convert(ws)
    