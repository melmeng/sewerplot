import os
import pandas as pd
import logging

logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)

def merge_csv(file_list, out_csv):
    '''
    Input file must follow the format,
    Time,Seconds,100008.1,100047.1,100683.1,100750.1,100979.1,101331.1,1
    [Hr:Min:s],[s], ds_depth [ft], ds_depth [ft], ds_depth [ft], ds_dept
    03/23/2013 00:00:00,                   0,     0.49160,     0.14813, 
    03/23/2013 00:50:00,                3000,     0.46998,     0.14523,
    '''
    df_merged = None
    logging.info('Merging the follwing files:')
    for f in file_list:
        logging.info('    %s' % f)
        df = pd.read_csv(f, index_col=0, parse_dates=True, skiprows=[1])
        if df_merged is None:
            df_merged = df
        else:
            df_merged = df_merged.append(df)
    df_merged['_idx'] = df_merged.index
    df_merged = df_merged.sort(['_idx'])
    del df_merged['_idx']
    
    df_merged.to_csv(out_csv)
    logging.info('Merged file saved to: %s' % out_csv)
    
def process_run(folder):
    '''
    merge all exported csv file in the run folder into 3 files:
    _flow.csv
    _depth.csv
    _velocity.csv
    '''
    logging.info('Mergeing simulation files in folder: %s' % folder)
    files = {}
    for f in os.listdir(folder):
        if f.startswith('_'):
            pass #ignore any files starts with "_"
        else:
            for k in ['depth.csv', 'flow.csv', 'vel.csv']:
                if k in f:
                    files.setdefault(k, [])
                    files[k].append(f)
    for k in files:
        merge_csv([os.path.join(folder, f) for f in files[k]], os.path.join(folder, '_%s'  % k))
            

class Workspace:
    def __init__(self, workspace):
        self.workspace = workspace
        self.data = {}
        self.load_flow_survey_group()
    def __getitem__(self, k):
        return self.data[k]
    def load_flow_survey_group(self):
        folder = os.path.join(self.workspace, 'flow_survey_group')
        self.data['obs'] = {}
        for p in 'Depth Flow Velocity'.split():
            f = 'Observed %s Event.csv' % p
            f = os.path.join(folder, f)
            if os.path.exists(f):
                self.data['obs'][p.lower()] = f
    def process_run_group(self):
        
        folder = os.path.join(self.workspace, 'run_group')
        logging.info('processing run group: %s' % folder)
        for run in os.listdir(folder):
            if not os.path.isfile(run):
                process_run(os.path.join(folder, run))
                