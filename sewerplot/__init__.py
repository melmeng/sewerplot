import diurnal_pattern
import flow_scatter
import check_flow_data
import workspace
import os
import ts

'''
Catchment
--Network
--Flow Survey Group
--Graph Template Group
--Rainfall Group
--Run Group
--Selection List Group
--Waste Water Group
'''


DATA = {'obs': {'depth': 'Observed Depth Event.csv',
                'flow': 'Observed Flow Event.csv',
                'velocity': 'Observed Velocity Event.csv'
                }}
class SewerPlot:
    def __init__(self, workspace):
        self.workspace = workspace
        self.data = {}
        self.load_flow_survey_group()
    def __getitem__(self, k):
        return self.data[k]
    def load_flow_survey_group(self):
        folder = os.path.join(self.workspace, 'flow_survey_group')
        self.data['obs'] = {}
        for p in 'Depth Flow Velocity'.split():
            f = 'Observed %s Event.csv' % p
            f = os.path.join(folder, f)
            if os.path.exists(f):
                self.data['obs'][p.lower()] = f
                
                
            
        
    