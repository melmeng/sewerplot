import pandas as pd
import matplotlib.pyplot as plt
import datetime
import math
import pylab
import csv
import os
import logging
import plots
import sewerplot
import obs_vs_sim
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def cal_scatter_plot_tool(ws):
    cal_folder = os.path.join(ws, 'output')
    out_folder = os.path.join(ws, 'output\plots')
    logging.info('Generating calibration plots: [%s]->[%s]' % (cal_folder, out_folder))
    
    i = 0
    for event_type in ['flow', 'volume', 'depth_not_surcharged', 'depth_surcharged']:
        summary_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        
        if os.path.exists(summary_path):
            logging.info('Reading calibratin stats: %s' % summary_path)
        else:
            logging.warning('File does not exist: %s' % summary_path)
            continue
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        
        plots.cal_scatter_plot(summary_path, event_type, ax)
        
        fpath = os.path.join(out_folder, '%s_scatter.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()
        
def run(ws):
#     obs_vs_sim.run(ws)
    cal_scatter_plot_tool(ws)
    cal_folder = os.path.join(ws, 'output')
    out_folder = os.path.join(ws, 'output\plots')
    logging.info('Generating calibration plots: [%s]->[%s]' % (cal_folder, out_folder))
    plots.cal_hist_plot(cal_folder, out_folder)
    

if __name__ == '__main__':
    ws = r'C:\projects\Raleigh\crabtree_recalibration\doc\3 basins\obs_vs_sim'
    run(ws)
#     