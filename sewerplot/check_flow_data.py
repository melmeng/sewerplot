import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import numpy as np
import pylab
from pandas.tools.plotting import scatter_matrix
logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def check_data(f, start, end, meters, outfolder=None):
    df = pd.read_csv(f, parse_dates=True, index_col=0)
    df = df[start:end]
    #get the meters
    df = df.loc[:, meters]
    df.boxplot()
    if outfolder:
        f = os.path.join(outfolder, 'boxplot.png')
        plt.savefig(f)
    # #scatter matrix
    scatter_matrix(df)#  diagonal='kde',)
    if outfolder:
        f = os.path.join(outfolder, 'scatter.png')
        plt.savefig(f)
    
    # #plot flow data\
    df.plot(subplots=True)
    if outfolder:
        f = os.path.join(outfolder, 'flow.png')
        plt.savefig(f)
    
    df.hist()
    if outfolder:
        f = os.path.join(outfolder, 'hist.png')
        plt.savefig(f)
    
    plt.show()





