import pandas as pd
import numpy as np
import logging

FN = {'mean': np.mean, 'sum': np.sum, 'max': np.max, 'min': np.mean}

def stat(df, start, end, fns, out_csv, FN=FN, save_file=False):
    
    
    if 'Seconds' in df.columns:
        del df['Seconds']
    
    data = {}
    for col, fn in fns.items():
        data[col] = df[start:end].apply(FN[fn])
    df_stat = pd.DataFrame(data)
    logging.info('%s->%s' % (start, end))
    if save_file:
        df_stat.to_csv(out_csv)
    return df_stat