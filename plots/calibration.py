import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import matplotlib
import numpy as np
import pylab
from dateutil.parser import *

#scatter plot types
SCATTER_PLOT_TYPE = ['flow', 'volume', 'depth_not_surcharged', 'depth_surcharged']

#scatter plot volume goal lines
SCATTER_PLOT_VOLUME_LINES = [[1.25 ,'g--', 'Calibration Goal(-15% to +25%)'],
                      [0.85, 'g--', None],
                      [1, 'k-', 'Theoretical']
                        ]
#scatter plot volume goal lines
SCATTER_PLOT_FLOW_LINES = [[1.2 ,'g--', 'Calibration Goal(-10% to +20%)'],
                      [0.9, 'g--', None],
                      [1, 'k-', 'Theoretical']
                        ]


STYLES = ['ko', 'b^', 'gd', 'rs', 'mp', 'y+']

#global font settings
font = {'family' : 'normal',
        'weight' : 'semibold',#medium
        'size'   : 18}

matplotlib.rc('font', **font)

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

def hist_plot(ax, labels, values, xlabel, ylabel, title, ct):
    '''
    plot the scatter plot and the histogram plot
    '''
    
    
    
    width = 1
    ind = np.arange(len(labels))
    y = values
    
    rects = ax.bar(ind + width, y, width, align='center')
    def autolabel(rects):
        # attach some text labels
        i = 0
        for rect in rects:
            
            height = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., 1+height, '%d\n(%.1f%%)'% (ct[i], height),
                    ha='center', va='bottom')
            i+=1

    autolabel(rects)
    ax.set_xlim([min(ind+width) - 0.5, max(ind+width) + 0.5])
    ax.set_xticklabels(labels)
    ax.set_xticks(ind+width)
    def to_percent(y, position):
        # Ignore the passed in position. This has the effect of scaling the default
        # tick locations.
        return '%d'% y + '%'
    ax.yaxis.set_major_formatter(FuncFormatter(to_percent))
    ax.set_ylim(0, 100)
    ax.grid(True)
    ax.set_xlabel(xlabel)
    # ax.xaxis.set_label_coords(0.5, -0.15)
    #ax.xaxis.set_label_coords(1.05, -0.025)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    
    

def scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title, lw=2):
    for x, y, style,  label in points:
        ax.plot(x, y,style, label=label)
    ax.set(aspect=1)
    
    ax.set_xlim(0, ymax)
    ax.set_ylim(0, ymax)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ax.grid(True)
    
    #draw lines
    for x, y, style, label in lines:
        if label:
            ax.plot(x, y, style, lw=lw, label=label)
        else:
            ax.plot(x, y, style, lw=lw)

    ax.legend(loc='lower right', fontsize='small', numpoints=1)
    ax.set_title(title)
    
    




def dwf_scatter_plot(f_path, event_type, ax, label=None):
    '''
    plot the scatter plot
    '''
    
    df = pd.read_csv(f_path, index_col=2, parse_dates=True)
    event_list = df.groupby('start').groups.keys()
    
    event_list = sorted(event_list, key=lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %H:%M'))
    styles = dict(zip(event_list, STYLES))
    
    ymax = 0
    points = []
    
        
    title = 'DWF Calibration - Monitor-to-Model %s Comparision' % event_type.title()

    for event in event_list:
        criterion = df['start'].map(lambda x: x==event)
        d = df[criterion]
        
        x = d['obs_adj']
        y = d['sim']
        style = styles[event]
        if label:
            label = '%s(%s)' % (label, len(x.values))
        else:
            label = 'DWF: %s' % datetime.datetime.strptime(event, '%m/%d/%Y %H:%M').strftime('%m/%d/%Y')
        points.append((x, y, style, label))
        ymax = max(max(y), max(x), ymax)
        
    ymax = ymax*1.1    
    
    if event_type=='volume':
        xlabel = 'Observed Volume (MG)'
        ylabel = 'Predicted Volume (MG)'
        lines = [((0, ymax), (0, ymax*1.1),'g--', 'Calibration Goal(-10% to +10%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
        
    elif event_type in ['depth', 'depth_surcharged']:
        xlabel = 'Observed Depth (ft)'
        ylabel = 'Predicted Depth (ft)'
        surcharged = (event_type=='depth_surcharged')
        if surcharged:
            DL = -0.3
            DU = 1.5
            l_label = 'Calibration Goal(-0.3 to +1.5 feet)'
        else:
            DL = -0.3
            DU = 0.3
            l_label = 'Calibration Goal(-0.3 to +0.3 feet)'
        lines = [((0, ymax), (DU, ymax+DU),'g--', l_label),
             ((0, ymax), (DL, ymax+DL), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    elif event_type=='flow':
        xlabel = 'Observed Peak Flow (MGD)'
        ylabel = 'Predicted Peak Flow (MGD)'
        lines = [((0, ymax), (0, ymax*1.1),'g--', 'Calibration Goal(-10% to +10%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title='')


def wwf_scatter_plot(f_path, events, event_type, ax, label=None):
    '''
    plot the scatter plot
    '''

    #read the csv table
    df = pd.read_csv(f_path, parse_dates=True)
    #event_list is a dictionary: {'0404': '04/04/2013 12:00',..}
    event_list = dict([(x.replace('event', ''), events['events'][x][0]) for x in events['events'].keys()])
    styles = dict(zip(event_list.keys(), STYLES))
    
    ymax = 0
    points = []

    for event in event_list.keys():

        d = df
        #need to filter the data for ones marked as '--'
        x = d['obs_%s' % event]
        y = d['sim_%s' % event]
        xy = [(float(a), float(b))for a, b in zip(x, y) if a!='--']
        ct = len(xy)
        if ct==0:
            logging.warning('No record found for %s, exit')
            return
        x = [a for a, b in xy]
        y = [b for a, b in xy]
        style = styles[event]
        if label:
            l = label
        else:
            l = '%s (%s)' % (parse(event_list[event]).strftime('%m/%d/%Y'), ct)
        
        points.append((x, y, style, l))
        ymax = max(max(y), max(x), ymax)
        
    ymax = ymax*1.1    
    
    if event_type=='volume':
        xlabel = 'Observed Volume (MG)'
        ylabel = 'Predicted Volume (MG)'
        lines = [((0, ymax), (0, ymax*SCATTER_PLOT_FLOW_LINES[0][0]),SCATTER_PLOT_FLOW_LINES[0][1], SCATTER_PLOT_FLOW_LINES[0][2]),
             ((0, ymax), (0, ymax*SCATTER_PLOT_FLOW_LINES[1][0]), SCATTER_PLOT_FLOW_LINES[1][1], SCATTER_PLOT_FLOW_LINES[1][2]),
             ((0, ymax), (0, ymax*SCATTER_PLOT_FLOW_LINES[2][0]), SCATTER_PLOT_FLOW_LINES[2][1], SCATTER_PLOT_FLOW_LINES[2][2])]
        
    elif event_type in ['depth_not_surcharged', 'depth_surcharged']:
        xlabel = 'Observed Depth (ft)'
        ylabel = 'Predicted Depth (ft)'
        surcharged = (event_type=='depth_surcharged')
        if surcharged:
            DL = -0.3
            DU = 1.5
            l_label = 'Calibration Goal(-0.3 to +1.5 feet)'
        else:
            DL = -0.3
            DU = 0.3
            l_label = 'Calibration Goal(-0.3 to +0.3 feet)'
        lines = [((0, ymax), (DU, ymax+DU),'g--', l_label),
             ((0, ymax), (DL, ymax+DL), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    elif event_type=='flow':
        xlabel = 'Observed Peak Flow (MGD)'
        ylabel = 'Predicted Peak Flow (MGD)'
        lines = [((0, ymax), (0, ymax*SCATTER_PLOT_VOLUME_LINES[0][0]),SCATTER_PLOT_VOLUME_LINES[0][1], SCATTER_PLOT_VOLUME_LINES[0][2]),
             ((0, ymax), (0, ymax*SCATTER_PLOT_VOLUME_LINES[1][0]), SCATTER_PLOT_VOLUME_LINES[1][1], SCATTER_PLOT_VOLUME_LINES[1][2]),
             ((0, ymax), (0, ymax*SCATTER_PLOT_VOLUME_LINES[2][0]), SCATTER_PLOT_VOLUME_LINES[2][1], SCATTER_PLOT_VOLUME_LINES[2][2])]
    
    scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title='')
    
def dwf_3_scatter_plot(cal_folder, out_folder, label):
    logging.info('Generating DWF plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        if os.path.exists(f_path):
            dwf_scatter_plot(f_path, event_type, ax, label)
            fpath = os.path.join(out_folder, '%s_scatter.png' % event_type)
            fig.savefig(fpath, dpi=300, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()    
            pylab.close()
        else:
            logging.warning('File not found: %s' % f_path)
        


def wwf_3_scatter_plot(summary_csv_folder, out_folder, label, events):
    logging.info('Scatter Plot: %s->%s' % (summary_csv_folder, out_folder))
    i = 0
    for event_type in SCATTER_PLOT_TYPE:
        logging.info('Plot: %s' % event_type)
        f_path = os.path.join(summary_csv_folder, '%s_summary.csv' % event_type)
        logging.info('Reading summary csv: %s' % f_path)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        wwf_scatter_plot(f_path, events, event_type, ax, label)
        
        fpath = os.path.join(out_folder, '%s_scatter.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()        


def dwf_3_hist_plot(cal_folder, out_folder):
    logging.info('Generating DWF hist plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        df = pd.read_csv(f_path, index_col=2, parse_dates=True)
        
        n = int(df['error_p'].count())
        logging.info('%s data points.' % n)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        xlabel = 'Error (%)'
        ylabel = 'Frequency (%)'
        title = ''
        MAX = 10**10
        if event_type=='volume':
            ranges = [-MAX,-25,-10, 10,25, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = df['error_p']
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        elif event_type=='flow':
            ranges = [-MAX,-25,-10, 10,25, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = df['error_p']
            
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        elif event_type=='depth':
            ranges = [-MAX,-2,-0.3,0.3,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]
            values = []
            d = df['error']
            
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        else:
            logging.warning('Not implemented')
            return
            
        hist_plot(ax, labels, values, xlabel, ylabel, title, n)
        fpath = os.path.join(out_folder, '%s_hist.csv' % event_type)
        hist_df = pd.DataFrame({'labels': labels, 'pct': values, 'ct': [x/100.0*n for x in values]})
        hist_df.to_csv(fpath, index=False)
        fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()    
        

def wwf_3_hist_plot(summary_csv_folder, out_folder, events):

    logging.info('Histogram: %s->%s' % (summary_csv_folder, out_folder))
    #get the events
    event_list = dict([(x.replace('event', ''), events['events'][x][0]) for x in events['events'].keys()])

    i = 0

    for event_type in SCATTER_PLOT_TYPE:
        f_path = os.path.join(summary_csv_folder, '%s_summary.csv' % event_type)
        logging.info('Summary csv: %s' % f_path)
        df = pd.read_csv(f_path, parse_dates=True)
        error_list = []
        errorp_list = []
        for event in event_list.keys():
            for v in list(df['error_%s' % event]):
                try:
                    v = float(v)
                    error_list.append(v)
                except:
                    pass
            for v in list(df['errorp_%s' % event]):
                try:
                    v = float(v)
                    errorp_list.append(v)
                except:
                    pass
        
        n = len(error_list)
        if n==0:
            logging.warning('no records found for %s' % event_type)
            return

        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        xlabel = 'Deviation (%)'
        ylabel = 'Frequency (%)'
        title = ''

        MAX = 10**10

        if event_type=='volume':
            ranges = [-MAX,-50,-30,-10,20,30,50, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = errorp_list
            n = len(d)
            logging.info('%s %s points.' % (n, event_type))
            for a, b in ranges:
                ct = len([x for x in d if x>=a and x<b])/float(n)*100 #  int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s(%s)' % (a, b, ct*n/100.0, ct))
                values.append(ct)
        elif event_type=='flow':
            #TODO:move the settings to global variables
            ranges = [-MAX,-50,-30,-15,25,40,50, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = errorp_list
            n = len(d)
            logging.info('%s %s points.' % (n, event_type))
            for a, b in ranges:
                ct = len([x for x in d if x>=a and x<b])/float(n)*100 #  int(d[(d>= a) & (d<b)].count())/float(n)*100#ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s(%s)' % (a, b, int(ct/100.0*n), ct))
                values.append(ct)
        elif event_type=='depth_not_surcharged':
            xlabel = 'Deviation (ft)'
            ranges = [-MAX,-2,-0.3,0.3,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]
            values = []
            d = error_list
            n = len(d)
            logging.info('%s %s points.' % (n, event_type))
            for a, b in ranges:
                ct = len([x for x in d if x>=a and x<b])/float(n)*100 #  int(d[(d>= a) & (d<b)].count())/float(n)*100#ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s(%s)' % (a, b, int(ct/100.0*n), ct))
                values.append(ct)
        elif event_type=='depth_surcharged':
            xlabel = 'Deviation (ft)'
            ranges = [-MAX,-2,-0.3,1.67,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]
            values = []
            d = error_list
            n = len(d)
            logging.info('%s %s points.' % (n, event_type))
            for a, b in ranges:
                ct = len([x for x in d if x>=a and x<b])/float(n)*100 #  int(d[(d>= a) & (d<b)].count())/float(n)*100#ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s(%s)' % (a, b, int(ct/100.0*n), ct))
                values.append(ct)
        else:
            logging.warning('Not implemented')
            return

        # plt.tight_layout()
        hist_plot(ax, labels, values, xlabel, ylabel, title, [int(x/100.0*n) for x in values])#hist_plot(ax, labels, values, xlabel, ylabel, title, n)
        
        fpath = os.path.join(out_folder, '%s_hist.csv' % event_type)
        hist_df = pd.DataFrame({'labels': labels, 'pct': values, 'ct': [int(x/100.0*n) for x in values]})
        hist_df.to_csv(fpath, index=False)
        fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()     
        
def hist_plot3(cal_folder, out_folder):
    logging.info('Generating DWF hist plots')

    for event_type in ['flow', 'volume', 'depth', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_hist.csv' % event_type)
        if os.path.exists(f_path):
            logging.info('Reading calibratin stats: %s' % f_path)
            df = pd.read_csv(f_path)
    
            
            fig = plt.figure(figsize=(10,10))
            ax = fig.add_subplot(111)
            xlabel = 'Deviation'
            ylabel = 'Frequency (%)'
            title = ''
            
            labels = df['labels'].values
            labels = [x.replace('\r', '') for x in labels]
            
            n = float(df['ct'].sum())
            ct = df['ct'].values
            values = df['pct'].values
                
            hist_plot(ax, labels, values, xlabel, ylabel, title, ct)
            
            fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
        
            fig.savefig(fpath, dpi=300, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()    
            pylab.close()    