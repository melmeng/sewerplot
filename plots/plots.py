import math
import csv
import os
import datetime
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange, num2date
from matplotlib.ticker import FuncFormatter
import numpy as np
import pylab



def hist_plot_csv(csv_path, png_path=None, xlabel = 'Deviation', ylabel = 'Frequency (%)', title = ''):
    '''
    csv format:
    ct    labels    pct
    12    <-50%    6.703910615
    1    "-50%
    to
    -30%"    0.558659218
    15    "-30%
    to
    -15%"    8.379888268
    50    "-15%
    to
    25%"    27.93296089
    41    "25%
    to
    40%"    22.90502793
    17    "40%
    to
    50%"    9.497206704
    43    >50%    24.02234637

    '''
    logging.info('Reading hist csv: %s' % csv_path)
    df = pd.read_csv(csv_path)
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111)
    labels = df['labels'].values
    labels = [x.replace('\r', '') for x in labels]
    n = float(df['ct'].sum())
    ct = df['ct'].values
    values = df['pct'].values
    hist_plot(ax, labels, values, xlabel, ylabel, title, ct)
    if png_path:
        fig.savefig(png_path, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % png_path)
        plt.clf()    
        pylab.close()    
    else:
        logging.info('No png_path provided, draw on screen.')
        plt.show()

def hist_plot(ax, labels, values, xlabel, ylabel, title, ct):
    '''
    histogram plot with labels
    each bar is labeled as '%d(%.1f%%)'% (ct[i], height)
    '''
    assert len(ct)>0
    
    width = 1
    ind = np.arange(len(labels))
    y = values
    
    rects = ax.bar(ind + width, y, width, align='center')
    def autolabel(rects):
        # attach some text labels
        i = 0
        for rect in rects:
            
            height = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., 2+height, '%d(%.1f%%)'% (ct[i], height),
                    ha='center', va='bottom')
            i+=1

    autolabel(rects)
    ax.set_xlim([min(ind+width) - 0.5, max(ind+width) + 0.5])
    ax.set_xticklabels(labels)
    ax.set_xticks(ind+width)
    def to_percent(y, position):
        # Ignore the passed in position. This has the effect of scaling the default
        # tick locations.
        return '%d'% y + '%'
    ax.yaxis.set_major_formatter(FuncFormatter(to_percent))
    ax.set_ylim(0, 100)
    ax.grid(True)
    ax.set_xlabel(xlabel)
    #ax.xaxis.set_label_coords(1.05, -0.025)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    
    

def scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title, lw=2):
    '''
    scatter plot
    '''
    for x, y, style,  label in points:
        ax.plot(x, y,style, label=label)
    ax.set(aspect=1)
    
    ax.set_xlim(0, ymax)
    ax.set_ylim(0, ymax)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    
    ax.grid(True)
    
    #draw lines
    for x, y, style, label in lines:
        if label:
            ax.plot(x, y, style, lw=lw, label=label)
        else:
            ax.plot(x, y, style, lw=lw)

    ax.legend(loc='lower right', fontsize='small', numpoints=1)
    ax.set_title(title)
    
    

STYLES = ['ko', 'b^', 'gd', 'rs', 'mp', 'y+']


def dwf_scatter_plot(f_path, event_type, ax, label=None):
    '''
    plot the scatter plot
    '''
    
    df = pd.read_csv(f_path, index_col=2, parse_dates=True)
    event_list = df.groupby('start').groups.keys()
    
    event_list = sorted(event_list, key=lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %H:%M'))
    styles = dict(zip(event_list, STYLES))
    
    ymax = 0
    points = []
    
        
    title = 'DWF Calibration - Monitor-to-Model %s Comparision' % event_type.title()

    for event in event_list:
        criterion = df['start'].map(lambda x: x==event)
        d = df[criterion]
        
        x = d['obs_adj']
        y = d['sim']
        style = styles[event]
        if label:
            label = '%s(%s)' % (label, len(x.values))
        else:
            label = 'DWF: %s' % datetime.datetime.strptime(event, '%m/%d/%Y %H:%M').strftime('%m/%d/%Y')
        points.append((x, y, style, label))
        ymax = max(max(y), max(x), ymax)
        
    ymax = ymax*1.1    
    
    if event_type=='volume':
        xlabel = 'Observed Volume (MG)'
        ylabel = 'Predicted Volume (MG)'
        lines = [((0, ymax), (0, ymax*1.1),'g--', 'Calibration Goal(-10% to +10%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
        
    elif event_type in ['depth', 'depth_surcharged']:
        xlabel = 'Observed Depth (ft)'
        ylabel = 'Predicted Depth (ft)'
        surcharged = (event_type=='depth_surcharged')
        if surcharged:
            DL = -0.3
            DU = 1.5
            l_label = 'Calibration Goal(-0.3 to +1.5 feet)'
        else:
            DL = -0.3
            DU = 0.3
            l_label = 'Calibration Goal(-0.3 to +0.3 feet)'
        lines = [((0, ymax), (DU, ymax+DU),'g--', l_label),
             ((0, ymax), (DL, ymax+DL), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    elif event_type=='flow':
        xlabel = 'Observed Peak Flow (MGD)'
        ylabel = 'Predicted Peak Flow (MGD)'
        lines = [((0, ymax), (0, ymax*1.1),'g--', 'Calibration Goal(-10% to +10%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title='')


def wwf_scatter_plot(f_path, event_type, ax, label=None):
    '''
    plot the scatter plot
    '''
    
    df = pd.read_csv(f_path, index_col=2, parse_dates=True)
    event_list = df.groupby('start').groups.keys()
    event_list = sorted(event_list, key=lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %H:%M'))
    styles = dict(zip(event_list, STYLES))
    
    ymax = 0
    points = []
    
        
    title = 'DWF Calibration - Monitor-to-Model %s Comparision' % event_type.title()

    for event in event_list:
        criterion = df['start'].map(lambda x: x==event)
        d = df[criterion]
        ct = len(d.index)
        x = d['obs_adj']
        y = d['sim']
        style = styles[event]
        if label:
            l = label
        else:
            l = 'RDII %s (%s)' % (datetime.datetime.strptime(event, '%m/%d/%Y %H:%M').strftime('%m/%d/%Y'), ct)
        
        points.append((x, y, style, l))
        ymax = max(max(y), max(x), ymax)
        
    ymax = ymax*1.1    
    
    if event_type=='volume':
        xlabel = 'Observed Volume (MG)'
        ylabel = 'Predicted Volume (MG)'
        lines = [((0, ymax), (0, ymax*1.2),'g--', 'Calibration Goal(-10% to +20%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
        
    elif event_type in ['depth', 'depth_surcharged']:
        xlabel = 'Observed Depth (ft)'
        ylabel = 'Predicted Depth (ft)'
        surcharged = (event_type=='depth_surcharged')
        if surcharged:
            DL = -0.3
            DU = 1.5
            l_label = 'Calibration Goal(-0.3 to +1.5 feet)'
        else:
            DL = -0.3
            DU = 0.3
            l_label = 'Calibration Goal(-0.3 to +0.3 feet)'
        lines = [((0, ymax), (DU, ymax+DU),'g--', l_label),
             ((0, ymax), (DL, ymax+DL), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    elif event_type=='flow':
        xlabel = 'Observed Peak Flow (MGD)'
        ylabel = 'Predicted Peak Flow (MGD)'
        lines = [((0, ymax), (0, ymax*1.25),'g--', 'Calibration Goal(-15% to +25%)'),
             ((0, ymax), (0, ymax*0.85), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title='')
    
def dwf_3_scatter_plot(cal_folder, out_folder, label):
    logging.info('Generating DWF plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        if os.path.exists(f_path):
            dwf_scatter_plot(f_path, event_type, ax, label)
            fpath = os.path.join(out_folder, '%s_scatter.png' % event_type)
            fig.savefig(fpath, dpi=300, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()    
            pylab.close()
        else:
            logging.warning('File not found: %s' % f_path)
        


def wwf_3_scatter_plot(cal_folder, out_folder, label):
    logging.info('Generating DWF plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        
        wwf_scatter_plot(f_path, event_type, ax, label)
        
        fpath = os.path.join(out_folder, '%s_scatter.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()        


def dwf_3_hist_plot(cal_folder, out_folder):
    logging.info('Generating DWF hist plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        df = pd.read_csv(f_path, index_col=2, parse_dates=True)
        
        n = int(df['error_p'].count())
        logging.info('%s data points.' % n)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        xlabel = 'Error (%)'
        ylabel = 'Frequency (%)'
        title = ''
        MAX = 10**10
        if event_type=='volume':
            ranges = [-MAX,-25,-10, 10,25, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = df['error_p']
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        elif event_type=='flow':
            ranges = [-MAX,-25,-10, 10,25, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
            values = []
            d = df['error_p']
            
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        elif event_type=='depth':
            ranges = [-MAX,-2,-0.3,0.3,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]
            values = []
            d = df['error']
            
            for a, b in ranges:
                ct = int(d[(d>= a) & (d<b)].count())/float(n)*100
                logging.info('Count in range(%s, %s): %s' % (a, b, ct))
                values.append(ct)
        else:
            logging.warning('Not implemented')
            return
            
        hist_plot(ax, labels, values, xlabel, ylabel, title, n)
        fpath = os.path.join(out_folder, '%s_hist.csv' % event_type)
        hist_df = pd.DataFrame({'labels': labels, 'pct': values, 'ct': [x/100.0*n for x in values]})
        hist_df.to_csv(fpath, index=False)
        fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()    
        

def cal_hist_plot(cal_folder, out_folder):
    logging.info('Generating WWF hist plots')
    
    i = 0
    for event_type in ['flow', 'volume', 'depth_not_surcharged', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)
        df = pd.read_csv(f_path, index_col=1, parse_dates=True)
        event_list = list(set([x.split('_')[1] for x in df.columns if '_' in x]))
        error = []
        errorp = []
        for event in event_list:
            
            for idx, row in df.iterrows():
                obs = row['obs_%s' % event]
                
                #treat 0 as an invalid value
                if math.isnan(obs) or obs==0:
                    logging.warning('%s, %s is %s' % (idx, event, obs))
                else:
                    error.append(row['error_%s' % event])
                    errorp.append(row['errorp_%s' % event])
            
    

        
        n = len(errorp)
        logging.info('%s data points.' % n)
        i+=1
        fig = plt.figure(figsize=(10,10))
        ax = fig.add_subplot(111)
        xlabel = 'Error (%)'
        ylabel = 'Frequency (%)'
        title = ''
        MAX = 10**10
        if event_type=='volume':
            ranges = [-MAX,-50,-30,-10,20,30,50, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
        elif event_type=='flow':
            ranges = [-MAX,-50,-30,-15,25,40,50, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%s%%\nto\n%s%%' % x for x in ranges]
            labels[0] = '<%s%%' % ranges[0][1]
            labels[-1] = '>%s%%' % ranges[-1][0]
        elif event_type=='depth_not_surcharged':
            ranges = [-MAX,-2,-0.3,0.3,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]    
        
        elif event_type=='depth_surcharged':
            
            ranges = [-MAX,-2,-0.3,1.67,2, MAX]
            ranges = zip(ranges[:-1], ranges[1:])
            labels = ['%sft\nto\n%sft' % x for x in ranges]
            labels[0] = '<%sft' % ranges[0][1]
            labels[-1] = '>%sft' % ranges[-1][0]  
        else:
            raise Exception('type: %s Not implemented' % event_type) 
        
        if 'depth' in event_type:
            data = error
        else:
            data = errorp
        values = []
        ct_labels = []
        for a, b in ranges:
            ct = len([x for x in data if x>=a and x<b])
            ct_labels.append(ct)
            values.append(ct/float(n)*100)
            logging.info('Count in range(%s, %s): %s' % (a, b, ct))
           
       
    
            
        hist_plot(ax, labels, values, xlabel, ylabel, title, ct_labels)#hist_plot(ax, labels, values, xlabel, ylabel, title, n)
        
        fpath = os.path.join(out_folder, '%s_hist.csv' % event_type)
        hist_df = pd.DataFrame({'labels': labels, 'pct': values, 'ct': [x/100.0*n for x in values]})
        hist_df.to_csv(fpath, index=False)
        fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
    
        fig.savefig(fpath, dpi=300, orientation='landscape')
        logging.info('figure saved: %s' % fpath)
        plt.clf()    
        pylab.close()     
        
def hist_plot3(cal_folder, out_folder):
    logging.info('Generating DWF hist plots')

    for event_type in ['flow', 'volume', 'depth', 'depth_surcharged']:
        f_path = os.path.join(cal_folder, '%s_hist.csv' % event_type)
        if os.path.exists(f_path):
            logging.info('Reading calibratin stats: %s' % f_path)
            df = pd.read_csv(f_path)
    
            
            fig = plt.figure(figsize=(10,10))
            ax = fig.add_subplot(111)
            xlabel = 'Deviation'
            ylabel = 'Frequency (%)'
            title = ''
            
            labels = df['labels'].values
            labels = [x.replace('\r', '') for x in labels]
            
            n = float(df['ct'].sum())
            ct = df['ct'].values
            values = df['pct'].values
                
            hist_plot(ax, labels, values, xlabel, ylabel, title, ct)
            
            fpath = os.path.join(out_folder, '%s_hist.png' % event_type)
        
            fig.savefig(fpath, dpi=300, orientation='landscape')
            logging.info('figure saved: %s' % fpath)
            plt.clf()    
            pylab.close()    
            


def cal_scatter_plot(summary_path, event_type, ax, label=None, date_fmt='%Y%m%d', title=None):
    '''
    plot the scatter plot
    summary_path: the summary csv file. eg. flow_summary.csv
    #the event must be using the format YYYYMMDD
    link    fm    obs_20130404    sim_20130404    error_20130404    errorp_20130404
    111355.1    CR_25    0.426001    0.46374    0.037739    8.858899392
    135416.1    CR_24    0.217001    0.35806    0.141059    65.00384791
    277515.1    CR_01    2.2075    2.18203    -0.02547    -1.153793884
    139540.1    CR_27    0    0.34059    0.34059    inf
    277516.1    CR_02    1.975833    1.94456    -0.031273    -1.582775467
    100750.1    CR_05    1.15    1.00702    -0.14298    -12.43304348
    
    event_type: flow, depth, volume, depth_surcharged
    '''
    
    df = pd.read_csv(summary_path, index_col=1, parse_dates=True)
    #get the event list
    event_list = list(set([x.split('_')[1] for x in df.columns if '_' in x]))
    #TOOD: need to standardize the format of time for event
#     event_list = df.groupby('start').groups.keys()
    event_list = sorted(event_list, key=lambda x: datetime.datetime.strptime(x, date_fmt)) #fixed time format
    styles = dict(zip(event_list, STYLES))
    
    ymax = 0
    points = []
    
    if title is None:
        title = 'Monitor-to-Model %s Comparision' % event_type.title()
    logging.info('Chart: %s, %s' % (title,event_type))
    
    for event in event_list:

        
        x = []
        y = []
        
        for idx, row in df.iterrows():
        
            x1 = row['obs_%s' % event]
            y1 = row['sim_%s' % event]
            #treat 0 as an invalid value
            if math.isnan(x1) or x1==0:
                logging.warning('%s, %s is %s' % (idx, event, x1))
            else:
                x.append(x1)
                y.append(y1)
        ct = len(x)

        style = styles[event]
        if label:
            l = label
        else:
            l = 'RDII %s (%s)' % (datetime.datetime.strptime(event, date_fmt).strftime('%m/%d/%Y'), ct)
        logging.info(l)
        points.append((x, y, style, l))
        ymax = max(max(y), max(x), ymax)
        
    ymax = ymax*1.1    
    
    if event_type=='volume':
        xlabel = 'Observed Volume (MG)'
        ylabel = 'Predicted Volume (MG)'
        lines = [((0, ymax), (0, ymax*1.2),'g--', 'Calibration Goal(-10% to +20%)'),
             ((0, ymax), (0, ymax*0.9), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
        
    elif event_type in ['depth_not_surcharged', 'depth_surcharged']:
        xlabel = 'Observed Depth (ft)'
        ylabel = 'Predicted Depth (ft)'
        surcharged = (event_type=='depth_surcharged')
        if surcharged:
            DL = -0.3
            DU = 1.5
            l_label = 'Calibration Goal(-0.3 to +1.5 feet)'
        else:
            DL = -0.3
            DU = 0.3
            l_label = 'Calibration Goal(-0.3 to +0.3 feet)'
        lines = [((0, ymax), (DU, ymax+DU),'g--', l_label),
             ((0, ymax), (DL, ymax+DL), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    elif event_type=='flow':
        xlabel = 'Observed Peak Flow (MGD)'
        ylabel = 'Predicted Peak Flow (MGD)'
        lines = [((0, ymax), (0, ymax*1.25),'g--', 'Calibration Goal(-15% to +25%)'),
             ((0, ymax), (0, ymax*0.85), 'g--', None),
             ((0, ymax), (0, ymax), 'k-', 'Theoretical')]
    
    scatter_plot(ax, points, ymax, xlabel, ylabel, lines, title='')
    
    
