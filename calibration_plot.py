import pandas as pd
import sewerplot as sp
import plots
import matplotlib.pyplot as plt
import os
from dateutil.parser import *
import logging
import os
import datetime
import csv
import logging
import math
from util import read_events

logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)

#TODO: remove not used functions


def plot_dwf():
    for event in ['weekday', 'weekend']:
        cal_folder = os.path.join(WS, 'calibration/%s/patched' % event)
        out_folder = os.path.join(WS, 'output/calibration/%s' % event)
        label = 'Dry-Weather %s Flow' % event.title()
        plots.calibration.dwf_3_scatter_plot(cal_folder, out_folder, label)


def plot_wwf():
    event = 'wwf'
    cal_folder = os.path.join(WS, 'calibration/%s/patched' % event)
    out_folder = os.path.join(WS, 'output/calibration/%s' % event)
    label = None
    plots.calibration.wwf_3_scatter_plot(cal_folder, out_folder, label)

    #plots.calibration.wwf_3_hist_plot(cal_folder, out_folder)

def plot_wwf_hist():
    event = 'wwf'
    cal_folder = os.path.join(WS, 'calibration/%s/patched' % event)
    out_folder = os.path.join(WS, 'output/calibration/%s' % event)


    plots.calibration.wwf_3_hist_plot(cal_folder, out_folder)

def plot_dwf_hist():
    for event in  ['weekday', 'weekend']:
        cal_folder = os.path.join(WS, 'calibration/%s/patched' % event)
        out_folder = os.path.join(WS, 'output/calibration/%s' % event)
        plots.calibration.dwf_3_hist_plot(cal_folder, out_folder)


def calibration_summary_patch(summary_folder, out_folder):
    for p in ['depth', 'volume', 'flow']:
        f = os.path.join(summary_folder, '%s_summary.csv' % p)
        logging.info('patching:%s' % f)
        df = pd.read_csv(f)
        df = df[df.action!="don't use"]
        def fn(x):


            if math.isnan(x['obs_adj']):
                return x['obs']
            else:
                return x['obs_adj']
        if p=='depth':
            #create surcharged
            surcharged = df[df.obs>df.dia]
            if not surcharged.empty:


                surcharged['obs_adj'] = surcharged.apply(fn, axis=1)
                surcharged['error'] = surcharged['sim'] - surcharged['obs_adj']
                surcharged['error_p'] = (surcharged['sim'] - surcharged['obs_adj'])/surcharged['obs_adj']*100

                surcharged.to_csv(os.path.join(out_folder, 'depth_surcharged_summary.csv'), index=False)
                logging.info('!!!SURCHARGED LOCATION FOUND!!!')
                logging.info('patched: %s ' % os.path.join(out_folder, 'depth_surcharged_summary.csv'))

            #create no surcharged
            d = df[df.obs<=df.dia]
            d['obs_adj'] = d.apply(fn, axis=1)
            d['error'] = d['sim'] - d['obs_adj']
            d['error_p'] = (d['sim'] - d['obs_adj'])/d['obs_adj']*100
            d.to_csv(os.path.join(out_folder, 'depth_summary.csv'), index=False)
            logging.info('patched: %s ' % os.path.join(out_folder, 'depth_summary.csv'))
        else:
            d = df
            d['obs_adj'] = d.apply(fn, axis=1)
            d['error'] = d['sim'] - d['obs_adj']
            d['error_p'] = (d['sim'] - d['obs_adj'])/d['obs_adj']*100
            d.to_csv(os.path.join(out_folder, '%s_summary.csv' % p), index=False)
            logging.info('patched: %s ' % os.path.join(out_folder, '%s_summary.csv' % p))



def merge_calibration_tables(cal_folder, out_folder):
    logging.info('Generating tables')
    logging.info('read folder: %s' % cal_folder)

    data = {}
    columns = set('fm')
    events = set()

    for event_type in ['flow', 'volume', 'depth']:
        f_path = os.path.join(cal_folder, '%s_summary.csv' % event_type)
        logging.info('Reading calibratin stats: %s' % f_path)


        df = pd.read_csv(f_path)

        for (fm, start), v in df.groupby(['fm', 'start']).groups.items():
            data.setdefault(fm, {})
            data[fm].setdefault(start, {})
            events.add(start)
            v = df[df.apply(lambda x: x['fm']==fm and x['start']==start, axis=1)]



            for fld in ['obs_adj', 'sim', 'error', 'error_p']:
                col = '%s_%s' % (event_type, fld)
                columns.add(col)
                data[fm][start][col] = float(v[fld])


    columns = list(columns)
    events = list(events)
    events = sorted(events, key=lambda x: datetime.datetime.strptime(x, '%m/%d/%Y %H:%M'))
    logging.info('Events: %s' % events)
    fms = sorted(data.keys())
    df = {}
    df['fm'] = sorted(fms)
    header = []
    with open(out_file, 'w') as o:
        writer = csv.writer(o, lineterminator='\n')

        row1 = ['catchment']
        row2 = ['']
        row3 = ['']
        for x in ['depth', 'flow', 'volume']:
            row1+= [x]*3*len(events)
            for x in ['obs', 'sim', 'error']:
                row2+=[x]*len(events)
                for e in events:
                    row3.append(e)
        #writer.writerow(',observed peak depth,,,,simulated peak depth,,,,peak depth error,,,,observed peak flowrate,,,,simulated peak flowrate,,,,peak flowrate error,,,,observed volume,,,,simulated volume,,,,volume error,,,'.split(','))
        #writer.writerow('Catchment Name,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6,3/12,4/4,4/28,5/6'.split(','))
        writer.writerow(row1)
        writer.writerow(row2)
        writer.writerow(row3)
        for fm in fms:
            row = [fm]
            for p in ['depth', 'flow', 'volume']:
                for a in ['obs_adj', 'sim', 'error']:
                    for e in events:

                        if a=='error':
                            if p=='depth':
                                a = 'error'
                            else:
                                a = 'error_p'

                        fld = '%s_%s' % (p, a)
                        try:
                            v = data[fm][e][fld]
                        except:
                            v = None
                        if a=='error':
                            try:
                                v = '%.1f' % float(v)
                            except:
                                v = ''
                        elif a=='error_p':
                            try:
                                v = '%.1f%%' % float(v)
                            except:
                                v = ''
                        else:
                            try:
                                v = '%.2f' % float(v)
                            except:
                                v = ''
                        row.append(v)
            writer.writerow(row)
    logging.info('write to file: %s' % out_file)



def plot_hist_from_files():
    for event in  ['weekday', 'weekend', 'wwf']:
        cal_folder = os.path.join(WS, 'output/calibration/%s' % event)
        out_folder = os.path.join(WS, 'output/calibration/%s' % event)
        plots.calibration.hist_plot3(cal_folder, out_folder)



if __name__ == '__main__':
    #TODO: clean it up and have a working example in the workspace

    #events
    event_csv = os.path.join(r'C:\Users\men73570\Desktop\papps\Documents\projects\2014 Raleigh\Report\Appendix D\wwf\input', 'events.csv')
    events = read_events(event_csv)

    #paths
    summary_csv_folder = r'C:\Users\men73570\Desktop\papps\Documents\projects\2014 Raleigh\Report\Appendix D\scatter_plot\input'
    out_folder = r'C:\Users\men73570\Desktop\papps\Documents\projects\2014 Raleigh\Report\Appendix D\scatter_plot\output'
    #scatter plot
    label = None

    plots.calibration.wwf_3_scatter_plot(summary_csv_folder, out_folder, label, events)
    #histogram
    plots.calibration.wwf_3_hist_plot(summary_csv_folder, out_folder, events)
    # plot_hist_from_files()
    #
    #
    #
    #     import pprint
    #     print data['CR_01'].keys()
    #     print pprint.pprint(data['CR_01'])

    # plot_dwf_hist()
    # plot_dwf()
    # plot_wwf()
    # plot_wwf_hist()

    #



    #
    # for event in  ['wwf', 'weekday', 'weekend']:
    #     #patch csv
    #     summary_folder = './raleigh/calibration/%s' % event
    #     out_folder = './raleigh/calibration/%s/patched' % event
    # #     calibration_summary_patch(summary_folder, out_folder)
    #
    #     #combine
    #     cal_folder = os.path.join(WS, 'calibration/%s/patched' % event)
    #     out_folder = os.path.join(WS, 'output/calibration/%s' % event)
    #     out_file = os.path.join(out_folder, '%s_stats.csv' % event)
    #     merge_calibration_tables(cal_folder, out_file)
