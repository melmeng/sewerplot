import os
import sewerplot as sp


if __name__=='__main__':
    workspace = './workspace'
    model = sp.SewerPlot(workspace)
    #get the paths of files
    event_csv = os.path.join(workspace, 'flow_scatter/events-wwf.csv')
    depth_csv = model['obs']['depth']
    velocity_csv = model['obs']['velocity']
    # the folder where all the files will be saved
    output_folder = os.path.join(workspace, 'flow_scatter/output')
    if os.path.exists(output_folder):
        pass
    else:
        os.makedirs(output_folder)
    # figure size, 4in by 4 in
    figsize = (4, 4)
    sp.flow_scatter.plot_all(event_csv, depth_csv, velocity_csv, output_folder, figsize)