import pandas as pd
import logging


logger = logging.getLogger()
FORMAT = '[%(levelname)s]%(filename)s %(lineno)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)


def read_events(event_csv):
    '''
    events.csv
    ==========
    Sample csv format,
    fm	link	rainfall	U/S End	dia	event0404	event0324
    CR_01	277515.1	3	No	12	4/4/2013  6:00 - 4/6/2013 6:00	03/24/2013 0:00 - 03/26/2013 0:00
    CR_02	277516.1	16	No	12
    CR_03	100979.1	6	No	12
    CR_04	100683.1	16	No	12
    CR_05	100750.1	2	No	12
    CR_06	119791.1	2	No	12
    CR_07	114685.1	8	No	12
    CR_08	107529.1	8	No	12
    '''
    logging.info('reading events: %s' % event_csv)
    df = pd.read_csv(event_csv)

    event_cols = [x for x in df.columns if x.startswith('event')]
    fm_list = []
    events = {}
    data = {}
    link = {}
    fm_data = {}
    for idx, row in df.iterrows():
        fm = row['fm']
        fm_list.append(fm)
        data.setdefault(fm, {})
        data[fm]['link'] = str(row['link'])
        link[str(row['link'])] = fm
        fm_data[fm] = str(row['link'])
        data[fm]['events'] = {}
        for event in event_cols:
            events.setdefault(event, [])
            if events[event]:
                pass
            else:
                start , end = [x.strip() for x in row[event].split('-')]
                events[event] = start, end

    logging.info('events: %s' % events)
    logging.info('link: %s' % link)
    return {'events': events, 'link': link, 'fm': fm_data}
